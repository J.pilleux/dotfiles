" Commands, functions and autocommands

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

fun! FormatPython()
    :! black %
endfun

let g:jpilleux_colorscheme = "tokyonight"

fun! SetColorScheme()
    let g:gruvbox_contrast_dark = 'hard'
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif

    set background=dark
    if has('nvim')
        call luaeval('vim.cmd("colorscheme " .. _A[1])', [g:jpilleux_colorscheme])
    else
        colorscheme gruvbox
    endif

    " highlight ColorColumn ctermbg=0 guibg=grey
    highlight Normal guibg=none ctermbg=none
    highlight LineNr guifg=#ff8659
    highlight LineNr guifg=#aed75f
    highlight LineNr guifg=#5eacd3
    highlight netrwDir guifg=#5eacd3
    highlight qfFileName guifg=#aed75f
    hi TelescopeBorder guifg=#5eacd
endfun
call SetColorScheme()

augroup J_PILLEUX
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
    " Automatic insert when entering a terminal
    autocmd TermOpen * startinsert
    " Automatic update Xresource
    autocmd BufWritePost Xresources !xrdb $HOME/.config/Xresources
    autocmd VimEnter * call SetColorScheme()
    " Show diagnostic popup on cursor hold
    autocmd CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics()
augroup END

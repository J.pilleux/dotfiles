" The theme comes in two styles, 'storm' and a darker variant 'night'.
let g:tokyonight_style = "night"

" Comments are italicized by default
let g:tokyonight_italic_comments = 0

" Keywords are italicized by default
let g:tokyonight_italic_keywords = 0

" Functions are not italicized by default
let g:tokyonight_italic_functions = 0

" Enable this to disable setting the background color
let g:tokyonight_transparent = 0

" Enabling this option, will hide inactive statuslines and
" replace them with a thin border instead. Should work with
" the standard `StatusLine` and `LuaLine`.
let g:tokyonight_hide_inactive_statusline = 1

syntax enable
filetype plugin indent on

set guicursor=
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set nohlsearch
set expandtab
set smartindent
set rnu nu
set nowrap
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set scrolloff=8
set signcolumn=yes
set isfname+=@-@
set termguicolors

set completeopt=menuone,noinsert,noselect
set shortmess+=c

" Reducing updatetime
set updatetime=50

set cmdheight=1

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=grey

set background=dark
colorscheme tokyonight


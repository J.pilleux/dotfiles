" Define the strategy used in file matching
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

" For Dashboard define what fuzzy finder to use
let g:dashboard_default_executive ='telescope'

" Here is the plugins to install for neo-vim
call plug#begin('~/.config/nvim/plugged')
    " Devicons
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'ryanoasis/vim-devicons'

    " Status bar themes
    Plug 'hoob3rt/lualine.nvim'
    " Plug 'vim-airline/vim-airline'

    " Themes
    Plug 'gruvbox-community/gruvbox'
    Plug 'sainnhe/gruvbox-material'
    Plug 'phanviet/vim-monokai-pro'
    Plug 'colepeters/spacemacs-theme.vim'
    Plug 'flazz/vim-colorschemes'
    Plug 'chriskempson/base16-vim'
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'colepeters/spacemacs-theme.vim'
    Plug 'folke/tokyonight.nvim'

    " Telescope
    " Main requirements (mandatories)
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    " Telescope itself
    Plug 'nvim-Telescope/telescope-fzy-native.nvim'

    " Code utilities
    Plug 'sbdchd/neoformat'
    Plug 'windwp/nvim-autopairs'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-commentary'

    " Language server protocol
    Plug 'neovim/nvim-lspconfig'
    Plug 'nvim-lua/lsp_extensions.nvim'
    Plug 'nvim-lua/completion-nvim'

    " Languages specific
    Plug 'neovimhaskell/haskell-vim'
    Plug 'tpope/vim-markdown'

    " Stuff
    Plug 'vim-utils/vim-man'
    Plug 'mbbill/undotree'
    Plug 'glepnir/dashboard-nvim'
    Plug 'dbeniamine/cheat.sh-vim'

    " Debuggers
    Plug 'puremourning/vimspector'
    Plug 'szw/vim-maximizer'

call plug#end()

let mapleader = " "

lua require('jpilleux')
lua require('nvim-autopairs').setup()

let loaded_matchparen = 1
if executable('rg')
    let g:rg_derive_root='true'
endif


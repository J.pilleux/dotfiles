local nvim_lsp = require'lspconfig'

local on_attach = function(client)
    require'completion'.on_attach(client)
end

-- rustup component add rust-src
nvim_lsp.rust_analyzer.setup({ on_attach=on_attach })

-- pacman -Sy clang
nvim_lsp.clangd.setup {
    on_attach = on_attach,
    root_dir = function() return vim.loop.cwd() end
}

-- pacman -S haskell-language-server
nvim_lsp.hls.setup { on_attach=on_attach }

-- maybe duplicate of the one above
nvim_lsp.hie.setup { on_attach=on_attach }

-- npm i -g bash-language-server
nvim_lsp.bashls.setup{ on_attach=on_attach }

-- pip install python-language-server
-- This require Jedi ( pip install jedi )
nvim_lsp.pyls.setup{ on_attach=on_attach }

-- npm install -g vscode-html-languageserver-bin
--Enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

nvim_lsp.html.setup {
  capabilities = capabilities,
  on_attach=on_attach
}

vim.lsp.handlers["textDocument/publicDiagnostics"] = vim.lsp.with (
    vim.lsp.diagnostic.on_publish_diagnostics, {
        virtual_text = true,
        signs = true,
        update_in_insert = true,
    }
)

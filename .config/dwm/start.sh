#!/bin/sh

curdate() {
    local cdate="$(date +"%d/%m %H:%M" -u)"
    echo $cdate
}

battery() {
    local batleft="$(cat /sys/class/power_supply/BAT1/capacity)"
    echo "$batleft %"
}

upgradable() {
    local nb_packages="$(pacman -Qu | wc -l)"
    echo "pac $nb_packages"
}

cpuusageperc() {
    local cpu="$(mpstat | tail -1 | sed 's/  */ /g' | cut -d' ' -f3 | cut -d'.' -f1)"
    echo "C $cpu %"
}

memusageperc() {
    local mem="$(/home/julien/.local/bin/memperc)"
    echo "M $mem %"
}

run_composer() {
    if ! command -v picom
    then
        compton
    else
        picom
    fi
}

run_composer &

if pgrep "$0" > /dev/null; then
    killall "$0"
fi &

while true; do
    feh --randomize --bg-scale /home/julien/.local/wallpapers/penguins/*
    sleep 1m
done &

while true; do
    if test -e '/sys/class/power_supply/BAT1/capacity'; then
        xsetroot -name " $(memusageperc) | $(cpuusageperc) | $(battery) | $(curdate)";
    else
        xsetroot -name " $(memusageperc) | $(cpuusageperc) | $(curdate)";
    fi
sleep 1s
done &

if test -e '/home/julien/.config/Xresources'; then
    xrdb '/home/julien/.config/Xresources'
fi &

exec dwm

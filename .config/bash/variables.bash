#
# Bash variables
#

# Idk what this does.
xhost +local:root > /dev/null 2>&1
complete -cf sudo

# This helps the "interactives shell displays" to properly render.
shopt -s checkwinsize

# Makes the defined alises expand the existing aliase list.
shopt -s expand_aliases

# Enable history appending instead of overwriting.
shopt -s histappend

# You know it, the only true useful option
set -o vi

# Path appending
HOMEBIN=${HOME}/.local/bin
test -d "${HOMEBIN}" && export PATH=${PATH}:${HOMEBIN}


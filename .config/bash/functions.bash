#
# Bash functions
#

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

updatecfg() {
    local check_file=/tmp/daily_cfg_update.file
    test ! -e ${check_file} && (
        echo -n "Updating rc configuration... "
        /usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME pull &> /dev/null
        test ${?} -eq 0 && echo -e "\033[32mDone\033[0m" || echo -e "\033[31mFailed\033[0m"
        touch ${check_file}
    )
}

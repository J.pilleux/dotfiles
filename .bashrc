#
# ~/.bashrc
#

# This bashrc NEEDS you to set the XDG_CONFIG_HOME bash variable. See the
# Arch wiki to get more details on this variable lol. It except the following
# structure :
# ${XFG_CONFIG_HOME}    (My personal choice : ${HOME}/.config/)
#       bash/
#           functions.bash
#           colors.bash
#       shell/
#           aliases     (In different folder to be common with other shells)

[[ $- != *i* ]] && return

# The most important part to flex and look cool on the internet.
if command -v neofetch &> /dev/null; then
    neofetch
elif command -v screenfetch &> /dev/null; then
    screenfetch
fi

BASHRC_RED="\033[31m"
BASHRC_GREEN="\033[32m"
BASHRC_YELLOW="\033[33m"
BASHRC_NO_COL="\033[0m"

bashrc_warning() {
    local msg=$1
    echo -e "-- ${BASHRC_YELLOW}Bashrc warning${BASHRC_NO_COL} : ${msg}"
}

source_part() {
    local file=$1
    if test -f "${file}"; then
        source "${file}"
    else
        bashrc_warning "${file} not found"
    fi
}

XDG_CONFIG_HOME="${HOME}/.config"
OVERRIDE_FILE="${HOME}/.override.bash"
FUNCTION_FILE="${XDG_CONFIG_HOME}/bash/functions.bash"
COLOR_FILE="${XDG_CONFIG_HOME}/bash/colors.bash"
VARIABLES_FILE="${XDG_CONFIG_HOME}/bash/variables.bash"
ALIASES_FILE="${XDG_CONFIG_HOME}/shell/aliases"

CARGO_ENV="${HOME}/.cargo/env"
AUTOJUMP_FILE="/etc/profile.d/autojump.sh"

source_part ${VARIABLES_FILE}
source_part ${FUNCTION_FILE}
source_part ${COLOR_FILE}
source_part ${ALIASES_FILE}
source_part ${OVERRIDE_FILE}
source_part ${CARGO_ENV}
source_part ${AUTOJUMP_FILE}

if test -e "${HOME}/.shinken-aliases"; then
    source ${HOME}/.shinken-aliases
fi

unset FUNCTION_FILE COLOR_FILE VARIABLES_FILE ALIASES_FILE

export HISTTIMEFORMAT="%d/%m/%y %T "
LOCALBIN=${HOME}/.local/bin
export PATH=${PATH}:${LOCALBIN}
